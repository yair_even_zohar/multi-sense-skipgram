#!/bin/sh

if [ ! -e CP.hack ]; then
    echo "Run make_cp.sh script first"
    exit    
fi

classpath=`cat CP.hack`
wordvec_app="java -Xmx8g -cp ${classpath} WordVec"

${wordvec_app}  --model=NP-MSSG --train=/home/yair/data/Twitter/tweets/2016-01-10_2016-01-11_1/trainClean1.txt --output=vectors-NP-MSSG.gz --sense=2 --learn-top-v=4000 --size=300 --window=5 --min-count=20  --threads=23  --negative=1 --sample=0.001 --binary=1 --ignore-stopwords=1 --encoding=ISO-8859-15 --save-vocab=twitter.np.vocab.gz --rate=0.025 --delta=0.1 --lowercase=false 

