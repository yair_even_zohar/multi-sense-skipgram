#!/bin/sh

if [ ! -e CP.hack ]; then
    echo "Run make_cp.sh script first"
    exit    
fi

classpath=`cat CP.hack`
wordvec_app="java -Xmx8g -cp ${classpath} WordVec"

# --train=/home/yair/data/Twitter/tweets/2016-01-10_2016-01-11_1/trainClean1.txt
${wordvec_app}  --model=MSSG-MaxOut --train=/home/yair/data/word2vec/onTweets/outTweets.txt --output=vectors-MSSG.gz --sense=1 --learn-top-v=100000 --size=300 --window=5 --min-count=20  --threads=23  --negative=1 --sample=0.001 --binary=1 --ignore-stopwords=1 --encoding=ISO-8859-15 --save-vocab=twitter.vocab.gz --rate=0.025 --delta=0.1 --lowercase=false 

