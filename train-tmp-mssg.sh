#!/bin/sh

if [ ! -e text8 ]; then
  wget https://dl.dropboxusercontent.com/u/39534006/text8.zip
  unzip text8.zip
fi

if [ ! -e CP.hack ]; then
    echo "Run make_cp.sh script first"
    exit    
fi

classpath=`cat CP.hack`
wordvec_app="java -Xmx100g -cp ${classpath} WordVec"

echo ${wordvec_app} 
${wordvec_app}  --model=MSSG-KMeans --train=wikiSmallCorpus.txt --output=vectors-wiki-MSSG.gz --sense=1 --learn-top-v=50000 --size=300 --window=5 --min-count=15  --threads=23  --negative=1 --sample=0.001 --binary=1 --ignore-stopwords=1 --save-vocab=wiki.vocab.gz --rate=0.025 --delta=0.1 --lowercase=false
