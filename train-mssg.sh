#!/bin/sh

if [ ! -e text8 ]; then
  wget https://dl.dropboxusercontent.com/u/39534006/text8.zip
  unzip text8.zip
fi

if [ ! -e CP.hack ]; then
    echo "Run make_cp.sh script first"
    exit    
fi

classpath=`cat CP.hack`
wordvec_app="java -Xmx14g -cp ${classpath} WordVec"

#--train=wikiSmallCorpus.txt --train=/home/yair/data/word2vec/onWikipedia/allWiki
echo ${wordvec_app} 
${wordvec_app}  --model=MSSG-MaxOut --train=wikiCorpusClean.all.txt --output=vectors.all.-MSSG.gz --sense=1 --learn-top-v=50 --size=300 --window=5 --min-count=25  --threads=23  --negative=1 --sample=0.001 --binary=1 --ignore-stopwords=1 --encoding=ISO-8859-15 --save-vocab=wiki.all.vocab.gz --rate=0.025 --delta=0.1 --lowercase=false
