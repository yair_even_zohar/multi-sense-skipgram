package utils

object CmdUtils {
	def parseArgs(args : Array[String]) : Map[String, String] = {
		Map[String, String]()
		.withDefault { x => throw new RuntimeException("Missing arg: '"+x+"'. Use '"+x+"=<value>'") } ++
		args.map { arg =>
				if (arg.contains("=")) {
					val ps = arg.split("=", 2)
					(ps(0).toLowerCase -> ps(1))
				} else if (arg.startsWith("-") && arg.length() > 1) {
					(arg.substring(1).toLowerCase -> "TRUE")
				} else {
					throw new RuntimeException("Bad arg: '" + arg + "'")
				}
			}.toMap
	}
}