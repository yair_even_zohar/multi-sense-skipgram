package utils

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.log4j.Level

object SparkUtils {
  	def initSparkConf(sparkConf : SparkConf) = {
		sparkConf.set("spark.kryo.referenceTracking", "false")
		sparkConf.set("spark.kryoserializer.buffer", "500m")
		sparkConf.set("spark.kryoserializer.buffer.max", "1000m")
//		    	sparkConf.set("spark.kryo.registrationRequired", "true")
		sparkConf.set("spark.driver.maxResultSize", "0") // unlimited
		sparkConf.registerKryoClasses(Array(
			classOf[scala.Tuple2[Any, Any]], classOf[Array[scala.Tuple2[Any, Any]]], classOf[scala.Tuple3[Any, Any, Any]], classOf[Array[scala.Tuple3[Any, Any, Any]]]
		))
		sparkConf
	}
  	
  	def createSparkContext() : SparkContext = {
		val sc = new SparkContext(initSparkConf(new SparkConf() ))
//		.setAppName("Semantics").setMaster("local[*]")))
		val hadoopConf = sc.hadoopConfiguration
		hadoopConf.set("fs.s3n.awsAccessKeyId", "AKIAI4GXGXUW62TWI6JA")
		hadoopConf.set("fs.s3n.awsSecretAccessKey", "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")
		sc
	}
  	
  	import org.apache.log4j.Level
  	import org.apache.log4j.Logger
  	def setLoggerLevel(level:Level = Level.WARN) : Unit = {
		Logger.getLogger("org").setLevel(level)
		Logger.getLogger("akka").setLevel(level)
  	}
  	
  	
}