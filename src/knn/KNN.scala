package knn

import scala.collection.mutable.PriorityQueue
import utils.SparkUtils

object KNN {
	def main(args : Array[String]) : Unit = {
  		if (args.length < 3) {
			System.err.println("Usage: <inputPath>  <outputPath> [K=20]")
			System.err.println("Usage Example: s3n://tk-research/word2vec/vectors-oneliners5  s3n://tk-research/word2vec/out/knn  20")
			return
		}
		val inputPath = args(0)
		val outputPath = args(1)
		val K = if (args.length > 2) args(2).toInt else 20
  		
		val sc = SparkUtils.createSparkContext()
		val input = sc.textFile(inputPath).map(toVectorPair).persist
		val bArray = sc.broadcast(input.collect)
		
		input.map(x=>(x._1,bArray.value.filterNot(_._1==x._1).aggregate(PriorityQueue[(String, Double)]()(dis))(
						(a, b) => cutToK(K, a += (b._1->cosd(x._2,b._2))),
						(a, b) => cutToK(K, a ++= b))
		))
		.map(x => x._1 + "\t" + x._2.toList.sortBy(_._2).reverse.map(y=>y._1+": ").mkString(", "))
		.saveAsTextFile(outputPath)
		
		sc.stop
	}


	def toVectorPair(x:String):(String,Array[Double]) = {
		val a = x.split("\t", 2);
		( a(0) -> normedL2(a(1).split(" ").map(_.toDouble) ) )
	}
  	
  	def cosd(x : Array[Double], y : Array[Double]): Double = {
		var $ = 0.0D
		for (i <- 0 to x.length-1) $ += x(i) * y(i)
		$
	}

	def normedL2(v : Array[Double]) : Array[Double] = {
		var s = 0.0D
		for (x <- v) s += x * x
		s = math.sqrt(s)
		for (i <- 0 to v.length-1) v.update(i, v(i) / s)
		v
	}
	private def dis() = new Ordering[(String, Double)] {
		def compare(a : (String, Double), b : (String, Double)) = -a._2.compare(b._2)
	}

	def cutToK(K:Int, $ : PriorityQueue[(String, Double)]) : PriorityQueue[(String, Double)] = {
		while ($.size > K) $.dequeue; $
	}
	
}