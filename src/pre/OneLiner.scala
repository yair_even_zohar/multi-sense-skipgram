package pre

import utils.SparkUtils

object OneLiner {
	def main(args : Array[String]) : Unit = {
		if (args.length < 3) {
			System.err.println("Usage: <inputPath>  <outputPath> <#Dimensions>")
			System.err.println("Usage Example: s3n://tk-research/word2vec/vectors-all-MSSG.bz2  s3n://tk-research/word2vec/vectors-oneliners5 300")
			return
		}
		val inputPath = args(0)
		val outputPath = args(1)
		val D = args(2).toInt-1
		val sc = SparkUtils.createSparkContext()
		val output = sc.textFile(inputPath)
			.zipWithIndex
			.map(x => (x._1, x._2.toInt - 1))
			.filter(x => (x._2 > 0) && (x._2 % 3 < 2))
			.map{x => (x._2 / 3 -> (x._2 % 3, x._1))}
			.groupByKey
			.map(x => x._2.toMap)
			.filter(m => m.contains(0)&&m.contains(1)&&"""^\S+ 1""".r.findFirstIn(m(0)).isDefined && ("""^\S+( \S+){"""+D+"}").r.findFirstIn(m(1)).isDefined || {
				println("BAD LINE: "+m.toString)
				false
			})
			.map(m => m(0).split(" ")(0) + "\t" + m(1))
		output.saveAsTextFile(outputPath)//, classOf[BZip2Codec])
		sc.stop
	}
}