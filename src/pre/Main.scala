package pre

import utils.SparkUtils
import org.apache.hadoop.io.compress.BZip2Codec

object Main {
  	def main(args : Array[String]):Unit = {
  		val sc = SparkUtils.createSparkContext()
  		val n = args(0).toInt.toString

		val bUnderScorer = sc.broadcast(new UnderScorer(
			sc.textFile("s3n://tk-ontology/resources/wikipedia/all_aliases"+n).map(_.toLowerCase).collect.toSet
  		))
		sc.textFile("s3n://tk-research/semantics/wikipedia.bz2")
			.map(preproc)
  			.map(bUnderScorer.value.underscoreAliases)
			.saveAsTextFile("s3n://tk-research/semantics/out"+n+"/wikipedia",classOf[BZip2Codec])

		sc.textFile("s3n://tk-research/rss/rss.txt.bz2")
			.map(preproc)
  			.map(bUnderScorer.value.underscoreAliases)
			.saveAsTextFile("s3n://tk-research/semantics/out"+n+"/rss",classOf[BZip2Codec])
			
		sc.stop
	}
  	
  	def preproc(s:String):String = {
  		"""[\.,;!?'_\-`\(\)\[\]\<\>:/\\]""".r.replaceAllIn(s, " ").replaceAll("\\s+", " ").trim
  	}
}