package pre

class UnderScorer(aliases : Set[String]) extends Serializable {
	def underscoreAliases(line : String): String = {
		val splitted = line.split(" ")
		var left = 0;
		var right = 0;
		val sb : StringBuilder = new StringBuilder
		while (left < splitted.size) {
			var current_str = splitted(left)
			var maxLengthFound = 0
			var longestAliasFound = ""
			for (j <- 0 to 5) {
				if (aliases.contains(current_str)) {
					maxLengthFound = j + 1
					longestAliasFound = current_str
				}
				if (left + j + 1 < splitted.size) {
					current_str += " " + splitted(left + j + 1)
				}
			}
			val len = if (longestAliasFound == "") 0 else longestAliasFound.split(" ").size
			if (longestAliasFound != "" && left + len >= right) {
				sb.append(longestAliasFound.replaceAll(" ", "_") + " ")
				right = left + len
			} else if (len == 0 && left >= right) {
				sb.append(splitted(left) + " ")
				right += 1
			}
			left += 1
		}
		sb.toString
	}
}
