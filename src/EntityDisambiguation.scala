// package cc.factorie.app.nlp.embeddings

import java.io._
import java.util.zip.GZIPInputStream

import cc.factorie.la.DenseTensor1
import cc.factorie.util.CmdOptions
import com.taykey.scala.framework.utils.JsonParser
import org.apache.spark

import scala.collection.JavaConversions._
import scala.collection.mutable
import scala.collection.mutable.{Set, MultiMap, PriorityQueue}
import scala.io.Source
import scala.util.control.Breaks
import scala.util.control.Breaks._


class ExtactOptions extends CmdOptions {
   val lowerCase = new CmdOption("lowercase", false, "BOOLEAN", "use lowercase only")
   val embedding = new CmdOption("embedding", "", "STRING", "use <string> for filename")
   val top       = new CmdOption("num-neighbour", 20, "INT", "use <int> to specify the k in knn")
}

object EntityDisambiguation {

  var threshold = 0
  var vocab         = Array[String]()
  var weights       = Array[Array[DenseTensor1]]()
  var ncluster      = Array[Int]()
  var nclusterCount = Array[Array[Int]]()
  var D = 0
  var V = 0
  var top = 25
  var S = 0
  var maxoutMethod = 0
  var lowerCase = false
  var  aliasToEntitesMap = new mutable.HashMap[String, Set[String]] with MultiMap[String, String]
  var  entittToCatTypeMap= new mutable.HashMap[String, Set[(String,String,String)]] with MultiMap[String, (String,String,String)]
  var  lowercaseAliasToEntitesMap = Map[String, Set[String]]()
  var  entitesToAliasMap = Map[String, Set[String]]()


  def main(args: Array[String]) {
    val opts  = new ExtactOptions()
    opts.parse(args)
    val embeddingFile       = opts.embedding.value
    top                     = opts.top.value


    loadEmbeddinfs(embeddingFile)
//    loadEntities("/home/yair/data/ontology/intelligenceOntology/ontology.json.gz", 10000) //("/home/yair/data/entities.json")
    updateEntities("/home/yair/data/ontology/iab_classification/wikiTitle_to_IAB_clean.tsv.gz")
    removeDictionaryAliases()
    //val list = aliasToEntitesMap.toList
//    lowercaseAliasToEntitesMap = Map[String, Set[String]]()
//    for (tuple <- aliasToEntitesMap.entrySet()){
//      val key = tuple.getKey
//      val value = tuple.getValue
//      lowercaseAliasToEntitesMap+=(key -> value.union( lowercaseAliasToEntitesMap.getOrElse(key,  Set[String]())))
//    }
    println ("Done loading entities")

    //if (args(3).toInt == 1) 
    //   displayKNN()          // given the context words, hard pick the sense and show the K-NN
    //else
//    play()
//    testApple()
//    displayKNN
    var filename = "/home/yair/data/Twitter/tweets/2016-01-10_2016-01-11_1/test1.txt"
//    filename="/home/yair/dev/workspace/multi-sense-skipgram/aliases/allAliases.json"
    filename ="/home/yair/dev/workspace/multi-sense-skipgram/wiki.all.vocab.gz"
    //disambigText("Sitting in the dark eating an applezba bumping Gucci, what is life")
    categorize(filename)
//    disambig(filename)
//    filename="/home/yair/dev/workspace/multi-sense-skipgram/wikiSmallCorpus.txt"              // given the word, show the K-NN of all the words
//    disambig(filename)
  }

  def removeDictionaryAliases(): Unit = {
    var dict: Set[String] = Set[String]()
    var lineItr = Source.fromFile("/home/yair/data/dictionary/wiki-100k.txt").getLines
    while (lineItr.hasNext) {
      val line = lineItr.next().stripLineEnd
      dict.add(line.toLowerCase)
    }

    for (alias <- aliasToEntitesMap.keys){
      val words = alias.split(" ")
      if (words.length > 1) {
        val entities = aliasToEntitesMap.get(alias).get.toList
        var categoriesSet: Set[String] = Set[String]()
        for (entity <- entities) {
          val set = entittToCatTypeMap.getOrElse(entity, Set())
          if (!set.isEmpty) {
            for ((iabCat, wikiCat, dbpedia_type) <- set) {
              categoriesSet.add(iabCat)
            }
          }
        }
        val categories = categoriesSet.toList
        var iab1Flag: Boolean = false
        var i = 0
        while (i < categories.size && iab1Flag == false) {
          if (categories(i).equals("IAB1") || categories(i).startsWith("IAB1-"))
            iab1Flag = true
          i += 1
        }
        if (iab1Flag) {
          var removeFlag: Boolean = true
          var j = 0
          while (j < words.size && removeFlag == true) {
            if (!dict.contains(words(j)))
              removeFlag = false
            j = j + 1
          }
          if (removeFlag)
            aliasToEntitesMap.remove(alias)
        }
      }
    }

  }
  def loadEntities( filename: String, minWikiCount:Int): Unit  = {

    // TODO - load wikiTitle_to_IAB.tsv from ontology directory and map each entity as it's own alias
    var lineItr = filename.endsWith(".gz") match {
      case false => Source.fromFile(filename).getLines
      case true => Source.fromInputStream(new GZIPInputStream(new FileInputStream(filename))).getLines
    }
    var counter = 0;
    while (lineItr.hasNext) {
      val line = lineItr.next()
      val node = JsonParser.fromJson(line)
      val wikiCount = node.get("wiki_count").asInt()
      if (wikiCount > minWikiCount) {
        val name = node.get("name").asText()
        //      val relevant = node.get("isRelevant").asBoolean()
        val nodeAliases = node.get("aliases")
        val alList = nodeAliases.elements().toList
        var aliases = Set[String]()
        aliases.add(name.toLowerCase)
        aliasToEntitesMap.addBinding(name.toLowerCase(), name)
        for (al <- alList) {
          val alias = al.asText()
          aliases.add(alias)
          aliasToEntitesMap.addBinding(alias, name)
        }
        if (!aliases.isEmpty)
          entitesToAliasMap += (name -> aliases)
        val nodeCategories = node.get("categories")
        val catList = nodeCategories.elements().toList
        for (cat <- catList) {
          val iabCat = cat.get("iab_category").asText
          val wikiCat = cat.get("wikipedia_category").asText
          val dbpedia_types = cat.get("dbpedia_type").elements().toList
          for (dbpedia_type <- dbpedia_types) {
            entittToCatTypeMap.addBinding(name, (iabCat, wikiCat, dbpedia_type.asText()))
          }
        }
      }
      counter = counter +1
      if (counter %10000 ==0)
        println(counter)
    }
    return
  }

  def updateEntities(filename: String): Unit ={
    var lineItr = filename.endsWith(".gz") match {
      case false => Source.fromFile(filename).getLines
      case true => Source.fromInputStream(new GZIPInputStream(new FileInputStream(filename))).getLines
    }
    var counter = 0;
    while (lineItr.hasNext) {
      val line = lineItr.next()
      val parts = line.split("\t")
      if (parts.length ==2) {
        val name = parts(0)
        if (!name.contains("(")) {
          aliasToEntitesMap.addBinding(name.replaceAll("_", " "), name)
          aliasToEntitesMap.addBinding(name.split('_').map(_.capitalize).mkString(" "), name)
          aliasToEntitesMap.addBinding(name.toLowerCase().replaceAll("_", " "), name)
          if (!entitesToAliasMap.contains(name)) {
            var aliases = Set[String]()
            aliases.add(name.replaceAll("_", " "))
            aliases.add(name.split('_').map(_.capitalize).mkString(" "))
            aliases.add(name.toLowerCase().replaceAll("_", " "))
            entitesToAliasMap += (name -> aliases)
          }
          if (!entittToCatTypeMap.contains(name))
            entittToCatTypeMap.addBinding(name, (parts(1), parts(1), parts(1)))
        }
      }
      counter = counter +1
      if (counter %10000 ==0)
        println("update - " +counter)
    }
  }

  def loadEmbeddinfs(embeddingsFile: String): Unit = {
    var lineItr = embeddingsFile.endsWith(".gz") match {
      case false => Source.fromFile(embeddingsFile).getLines
      case true => Source.fromInputStream(new GZIPInputStream(new FileInputStream(embeddingsFile)), "iso-8859-1").getLines
    }
    // first line is (# words, dimension)
    val details = lineItr.next.stripLineEnd.split(' ').map(_.toInt)
    V = if (threshold > 0 && details(0) > threshold) threshold else details(0)
    D = details(1)
    S = details(2)
    maxoutMethod = details(3)
    println("# words : %d , # size : %d".format(V, D))
    vocab = new Array[String](V)
    weights = Array.ofDim[DenseTensor1](V, S+1)
    ncluster = Array.ofDim[Int](V)
    nclusterCount = Array.ofDim[Int](V, S)
    for (v <- 0 until V) {
      val line = lineItr.next.stripLineEnd.split(' ')

      if (lowerCase)
        vocab(v) = line(0).toLowerCase
      else
        vocab(v) = line(0)
      ncluster(v) = if (line.size > 1) line(1).toInt else S
      if (line.size > 2) {
        for (i <- 0 until ncluster(v)) 
          nclusterCount(v)(i) = line(i + 2).toInt
      }
      for (s <- 0 until ncluster(v) + 1) {
        val line = lineItr.next.stripLineEnd.split(' ')
        weights(v)(s) = new DenseTensor1(D, 0) // allocate the memory
        for (d <- 0 until D) weights(v)(s)(d) = line(d).toDouble
        weights(v)(s) /= weights(v)(s).twoNorm
        if (s > 0 && maxoutMethod == 0) {
            val lineSkip = lineItr.next.stripLineEnd.split(' ')
        }
      }
      if(v%10000 ==0)
        println("loaded words = " + v)
    }
    println("loaded vocab and their embeddings" + ncluster(1626)+ 1)
  }

  def play(): Unit = {
    while (true) {
      print("Enter word (EXIT to break) : ")
      var word = readLine.stripLineEnd
//      findKNN(word)
      findEntityCategoryType(word, true)
    }
  }

  def categorize(vocabFile: String): Unit = {
    val writer = new PrintWriter(new File(vocabFile + ".out" ))
    val entropy = spark.mllib.tree.impurity.Entropy.instance
    var aliasCatMap = Map[String, (String, Double, Double)]()
    var aliasTypeTreeMap = Map[String, Double]()
    var jsonFlag:Boolean = false
    if (vocabFile.contains("json"))
      jsonFlag=true
    var lineItr = vocabFile.endsWith(".gz") match {
      case false => Source.fromFile(vocabFile).getLines
      case true => Source.fromInputStream(new GZIPInputStream(new FileInputStream(vocabFile)), "iso-8859-1").getLines
    }
    //for (alias <- aliasToEntitesMap.keySet) {
    while (lineItr.hasNext){
      val line = lineItr.next()
      var alias = line.split(" ")(0)
      if (jsonFlag){
        val node = JsonParser.fromJson(line)
        alias = node.get("alias_text").asText()
        val nodeDbTypes = node.get("dbpedia_type")
        val typeList = nodeDbTypes.elements().toList
        var types = Set[String]()
        for (typ <- typeList) {
            types.add(typ.asText())
          }
      }
//      if (aliasToEntitesMap.contains(alias)) {
        val (catMap, typeMap) = findEntityCategoryType(alias, false)
        var jsonMap = Map[String, scala.Any]()
        jsonMap += ("alias" -> alias)
        jsonMap += (" type" -> JsonParser.toJson(typeMap.toList sortBy {-_._2}))
        jsonMap += (" category" -> JsonParser.toJson(catMap.toList sortBy {-_._2}))
        writer.write(JsonParser.toJson(jsonMap))
        writer.write("\n")
        //Handle type
        //      val typeAarray = typeMap.values.toArray
        //      var summ = 0.0
        //      typeAarray.foreach(summ += _)
        //      val typeEntropy = entropy.calculate(typeMap.values.toArray, summ)
        //      val typeList = typeMap.toList sortBy {-_._2}
        //      if (typeList.size > 0) {
        //        val bestType = typeList(0)
        //        //aliasTypeMap += (alias ->(bestType._1, typeEntropy, summ))
        //        if (typeList.size==1)
        //          writer.write((alias + "\t" + bestType._1 +"\t" +  bestType._2 +  "\t" +
        //                            "\t"  +                   "\t" +  typeEntropy + "\t" + summ))
        //        else
        //          writer.write((alias + "\t" + bestType._1 +"\t" +  bestType._2  + "\t" +
        //            typeList(1)._1 +"\t"  +  typeList(1)._2  +"\t" + typeEntropy + "\t" + summ))
        //        writer.write("\n")
        //      }
        //handle category
        //      val array = catMap.values.toArray
        //      var sum = 0.0
        //      array.foreach(sum += _)
        //      val catEntropy = entropy.calculate(catMap.values.toArray, sum)
        //      val list = catMap.toList sortBy {-_._2}
        //      if (list.size > 0) {
        //        val bestCategory = list(0)
        //        //aliasCatMap += (alias ->(bestCategory._1, catEntropy, sum))
        //        if (list.size==1)
        //          println(alias + "\t" + bestCategory +"\t" + "\t" + catEntropy + "\t" + sum)
        //        else
        //          println(alias + "\t" + bestCategory + "\t" + list(1) +"\t" + catEntropy + "\t" + sum)
        //      }
//      }
    }
    writer.close()
  }

  def findEntityCategoryType(word: String, printOut: Boolean): (Map[String, Double],Map[String, Double]) ={
    //remove the last elemnt which is the alias itself
    val knn:List[(String,Double)] = findKNN(word, printOut).dropRight(1)
    var catTreeMap = Map[String, Double]()
    var typeTreeMap = Map[String, Double]()
    for ((w,score) <- knn) {
      val alias = w.replace("_", " ")
      val isAlias1 = aliasToEntitesMap.contains(alias)
//      val isAlias2 = lowercaseAliasToEntitesMap.contains(alias)
//      if (isAlias1 || isAlias2) {
//        var candidEntities = lowercaseAliasToEntitesMap.get(alias).get
        if (isAlias1){
          val candidEntities = aliasToEntitesMap.get(alias).get
        //  println(word + "\t" + candidEntities.mkString("__"))
        for (entity <- candidEntities) {
          val set = entittToCatTypeMap.getOrElse(entity, Set())
          if (!set.isEmpty) {
            for ((iabCat, wikiCat, dbpedia_type) <- set) {
              catTreeMap += (iabCat -> (score + catTreeMap.getOrElse(iabCat, 0.0)))
              typeTreeMap += (dbpedia_type -> (score + typeTreeMap.getOrElse(dbpedia_type, 0.0)))
            }
          }
        }
      }
    }
    if (printOut) {
      val catList = catTreeMap.toList sortBy {_._2}
      val typeList = typeTreeMap.toList sortBy {_._2}
      println(catList.mkString(" "))
      println(typeList.mkString(" "))
    }
    return (catTreeMap,typeTreeMap)
  }

  def findKNN(word: String, p: Boolean): List[(String,Double)] = {
      var knnList = List[(String,Double)]()
      val id  = getID(word)
      if (p)
        println("Id in the vocab for the word " + word + " " + id)
      if (id == -1 ) {
        if (p)
          println("words not in vocab")
      } else
      {
        val numCluster = ncluster(id)+ 1
        for (is <- 0 until ncluster(id)+ 1)
        {
          val embedding_in = weights(id)(is) // give out nearest neighbours for all the words
          var pq = new PriorityQueue[(String, Double)]()(dis)
          for (i <- 0 until vocab.size)
          {
             if (is > 0) {
             for (s <- 1 until ncluster(i) + 1)
             {
                  val embedding_out = weights(i)(s) // take only senses
                 val score = TensorUtils.cosineDistance(embedding_in, embedding_out)
                 if (pq.size < top) pq.enqueue(vocab(i) -> score)
                 else if (score > pq.head._2)
                 { // if the score is greater the min, then add to the heap
                  pq.dequeue
                  val vocab_str = vocab(i) //+ (if (ncluster(i) > 1) "_" + s.toString else "");
                  pq.enqueue(vocab_str -> score)
                 }
             }

             }
             if (is ==0) {
                val embedding_out = weights(i)(0) // take only senses
                 val score = TensorUtils.cosineDistance(embedding_in, embedding_out)
                 if (i < top) pq.enqueue(vocab(i) -> score)
                 else if (score > pq.head._2)
                 { // if the score is greater the min, then add to the heap
                  pq.dequeue
                  pq.enqueue(vocab(i) -> score)
                 }
             }
          }
        var arr = new Array[(String, Double)](pq.size)
        var i = 0
        while (!pq.isEmpty)
        { // min heap
          arr(i) = (pq.head._1, pq.head._2)
          i += 1
          pq.dequeue
        }
        if (p)
          print("\t\t\t\t\t\tWord\t\tCosine Distance")
        if (is==0 && p) {
          print("(Global Embedding)")
        }
        if (p) {
          print("\n")
          arr.reverse.foreach(x => println("%50s\t\t%f".format(x._1, x._2)))
        }
       knnList = arr.toList
      }
    }
    return knnList
  }
  def disambig(filename: String): Unit = {
    println("Start disambig")
    var lineItr = filename.endsWith(".gz") match {
      case false => Source.fromFile(filename).getLines
      case true => Source.fromInputStream(new GZIPInputStream(new FileInputStream(filename))).getLines
    }
    while (lineItr.hasNext){
     val line = lineItr.next()
     disambigText(line)
    }
  }

  def disambigText(line: String): Unit = {
    val words = line.stripLineEnd.split(' ').filter(word => getID(word) != -1)
    val int_words = words.map(word => getID(word)).filter(id => id!= -1)
    val embedding_in = new DenseTensor1(D, 0)
    println("words = " + words.mkString(" "))
    for (i <- 0 until words.size) {
      val w = int_words(i)
      val word = words(i)
      val contexts = int_words.filter(id => id != w)
      val isAlias = aliasToEntitesMap.contains(word)
//      println(word + " " + isAlias)
      //find if the alias has more then 1 possibilities (case indipendent)
      if (isAlias && lowercaseAliasToEntitesMap.getOrElse(word.toLowerCase, Set[String]()).size > 1) {
        val candidEntities = lowercaseAliasToEntitesMap.get(word.toLowerCase).get
//        println(word + "\t" + candidEntities.mkString("__"))
        //build the competing set
        var candidCatTypes = Map[Int, (String, String)]()
        for (entity <- candidEntities) {
          val set = entittToCatTypeMap.getOrElse(entity, Set())
//          println("set " +  entity + " = " + set.mkString(" "))
          if (!set.isEmpty) {
            for ((iabCat, wikiCat, dbpedia_type) <- set) {
              val competitor = iabCat + "_" + dbpedia_type.capitalize
//              println("competitor" + "=" + competitor)
              val catTypeEmbId = getID(competitor)
              candidCatTypes += catTypeEmbId ->(entity, competitor)
            }
          }
        }
        val entityEmbId = getCatType(candidCatTypes.keySet.toList, contexts)
        val (entity, competitor) = candidCatTypes.get(entityEmbId).get
        if (word.equalsIgnoreCase("apple"))
          println("---\t" + line.stripLineEnd + "\t" + entity + "\t" + competitor)
      }
    }
  }

  def displayKNN(): Unit = {
    while (true) {
      print("Enter the word : ")
      val in_word = readLine.stripLineEnd
      val w       = getID(in_word)
      print("Enter the contexts : ")
      val words = readLine.stripLineEnd.split(' ').filter(word => getID(word) != -1)
      val int_words = words.map(word => getID(word)).filter(id => id != -1)
      val contextEmbedding = new DenseTensor1(D)
      (0 until int_words.size).foreach(i => contextEmbedding.+=( weights(int_words(i))(0))    )
      contextEmbedding./=( contextEmbedding.twoNorm )
      val prob = new Array[Double](S+1)
      var z = 0.0
      for (s <- 1 until ncluster(w)+1) {
           val v = contextEmbedding.dot( weights(w)(s) )
           prob(s) = math.exp(v) * math.exp(v)
           //prob(s) = 1/(1+math.exp( -contextEmbedding.dot(weights(w)(s)) ))
           z += prob(s)
      }
      val emb = new DenseTensor1(D)
      for (s <- 1 until ncluster(w)+1) {
           println("Prob- " + s + " " + prob(s)/z)
           val x  = weights(w)(s) * prob(s)/z // multiply with the probablity
           emb.+=( x )
      }
      val nn = knn(emb, 1, S+1, 40)
       println("\t\t\t\t\t\tWord\t\tCosine Distance")
       nn.foreach(x => println("%50s\t\t%f".format(x._1, x._2)))
       println()
    }
    
  }

  def testApple(): Unit = {
    val filename = "/home/yair/data/Twitter/tweets/2016-01-10_2016-01-11_1/test.txt"
    val loop = new Breaks;
    for (line <- Source.fromFile(filename).getLines()) {
      val words = line.stripLineEnd.split(' ').filter(word => getID(word) != -1)
      val int_words = words.map(word => getID(word)).filter(id => id != -1)
      val embedding_in = new DenseTensor1(D, 0)
      breakable {
        for (i <- 0 until words.size) {
          val w = int_words(i)
          val word = words(i)
          val contexts = int_words.filter(id => id != w)
          val sense = getSense(w, contexts)
          embedding_in.+=(weights(w)(sense)) // sum up the local contexts find the closest word
          val arr = knn(weights(w)(sense))
          if (word.equalsIgnoreCase("apple")) {
            print(line.stripLineEnd + "\t" + sense + " " + word + " -")
            for (i<-0 until 9)print (" "+ arr(i)._1)
//            arr.foreach(x => print(" " + x._1))
            println()
            break
          }
        }
      }
//      val arr = knn(embedding_in)
//      println("\t\t\t\t\t\tWord\t\tCosine Distance")
//      arr.foreach(x => println("%50s\t\t%f".format(x._1, x._2)))
//      println()
    }
  }

  // private helper functions
  private def dis() = new Ordering[(String, Double)] {
    def compare(a: (String, Double), b: (String, Double)) = -a._2.compare(b._2)
  }
  private def getID(word: String): Int = {
    for (i <- 0 until vocab.length) if (vocab(i).equals(word))
      return i
    return -1
  }
  private def  getSense(word : Int, contexts : Seq[Int]): Int =  {
        val contextEmbedding = new DenseTensor1(D, 0)
        (0 until contexts.size).foreach(i => contextEmbedding.+=(weights(contexts(i))(0)) ) // global context
        var correct_sense = 0
        var max_score = Double.MinValue
        for (s <- 1 until ncluster(word)+1) {
             val score = contextEmbedding.dot( weights(word)(s) )// find the local context
             if (score > max_score) {
               correct_sense = s
               max_score = score
             }
//             print(s + " " + score + " " + logit(score) + ":")
        }
        correct_sense
  }
  private def  getCatType(catTypeEmbIds : List[Int], contexts : Seq[Int]): Int =  {
    val contextEmbedding = new DenseTensor1(D, 0)
    (0 until contexts.size).foreach(i => contextEmbedding.+=(weights(contexts(i))(0)) ) // global context
    var correct_catType = 0
    var max_score = Double.MinValue
    for (id <- catTypeEmbIds) {
      val score = contextEmbedding.dot( weights(id)(0) )// score =  context * global_weights(id)
      if (score > max_score) {
        correct_catType = id
        max_score = score
      }
      //             print(s + " " + score + " " + logit(score) + ":")
    }
    correct_catType
  }
  private def logit(x : Double):Double = 1/(1 + math.exp(-x))
  private def knn(in : DenseTensor1, st : Int = 1, en : Int = S+1, t : Int =10): Array[(String, Double)] = {
     var pq = new PriorityQueue[(String, Double)]()(dis)
     for (i <- 0 until vocab.size) 
     {  
       for (s <- 1 until ncluster(i)+1)  
       {
         val out = weights(i)(s) // take only senses 
         val score = TensorUtils.cosineDistance(in, out)
         if (i < top) pq.enqueue(vocab(i) -> score)
         else if (score > pq.head._2) 
         { // if the score is greater the min, then add to the heap
                  pq.dequeue
                  pq.enqueue(vocab(i) -> score)
         }
        }
      }
       var arr = new Array[(String, Double)](pq.size)
       var i = 0
       while (!pq.isEmpty) 
       { // min heap
          arr(i) = (pq.head._1, pq.head._2)
          i += 1
          pq.dequeue
       }
       arr = arr.reverse
       
      arr
   }


}
